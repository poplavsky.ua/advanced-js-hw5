// Завдання:
// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.

import { Card } from "./Card.js"

const fetchUsers = fetch("https://ajax.test-danit.com/api/json/users");
const fetchPosts = fetch("https://ajax.test-danit.com/api/json/posts");

Promise.all([fetchUsers, fetchPosts])
  .then((data) => {
    return Promise.all(data.map((d) => d.json()));
  })
  .then(([users, posts]) => {
    posts.forEach((post) => {
      const user = users.find((user) => post.userId === user.id);
      if (user) {
        const card = new Card(
          post.id,
          user.name,
          user.email,
          post.title,
          post.body
        );
        renderCard(card);
      }
    });
  })
  .catch((error) => console.log(error));

function renderCard(card) {
  const { id, name, email, title, body } = card;
  const addCard = document.querySelector('#root');
  addCard.insertAdjacentHTML(
    "beforeend",
    `
    <div class="card" data-id="${id}">
        <div class = "user_post_wrap">
            <div class = "user_info">
                <p class = "user_name">${name} <span class = "user_email">${email}</span></p>
                <button class = "post_delete" data-id="${id}">delete</button>
            </div>
            <div class = "post_info">
                <p class = "post_title">${title}</p>
                <p class = "post_body">${body}</p> 
            </div>
        </div>
    </div>`
  );

  const btnDelete = document.querySelector(`[data-id="${id}"]`);
  btnDelete.addEventListener("click", () => {
    deletePost(id);
  });
}

function deletePost(postId) {
  try {
    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
      method: "DELETE",
    }).then(() => {
      removePost(postId);
    });
  } catch (error) {
    console.log("Error deleting post:", error);
  }
}

function removePost(postId) {
  const card = document.querySelector(`[data-id="${postId}"]`);
  card.remove();
}
