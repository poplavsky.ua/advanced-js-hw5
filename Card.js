export class Card {
    constructor(id, name, email, title, body) {
      this.id = id;
      this.name = name;
      this.email = email;
      this.title = title;
      this.body = body;
    }
  }